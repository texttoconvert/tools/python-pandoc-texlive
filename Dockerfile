# Use an official Python runtime as a parent image
FROM pandoc/latex:latest

# Install python3
RUN apk --no-cache add python3 py3-pip bash findutils make && \
    rm -rf /var/cache/apk/*

# Change pyhton3 call to python
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN rm -rf /usr/bin/pip
RUN ln -s /usr/bin/pip3 /usr/bin/pip
