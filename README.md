# python-pandoc-texlive

## Docker base image for TextToConvert

- The image uses the python:latest.
- It adds pandoc and texlive latest versions available on the python:latest distribution.

Link to registry: [Docker image on Gitlab](https://gitlab.com/texttoconvert/tools/python-pandoc-texlive/container_registry)

## Use this image

### In docker file

```Dockerfile
FROM registry.gitlab.com/texttoconvert/tools/python-pandoc-texlive:latest
```
